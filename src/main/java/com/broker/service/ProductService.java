package com.broker.service;

import com.broker.model.Product;
import com.broker.model.dto.ProductDTO;
import com.broker.model.dto.mapper.ProductMapper;
import com.broker.repository.ProductRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository){
        this.productRepository = productRepository;
    }

    public ProductDTO saveProduct(Product product){

        return ProductMapper.fromProductToProductDTO(productRepository.saveAndFlush(product));
    }
}
