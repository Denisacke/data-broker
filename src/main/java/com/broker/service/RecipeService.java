package com.broker.service;

import com.broker.model.Recipe;
import com.broker.model.Product;
import com.broker.model.dto.RecipeDTO;
import com.broker.model.dto.CreateProductDTO;
import com.broker.model.dto.CreateRecipeDTO;
import com.broker.model.dto.mapper.CookedProductMapper;
import com.broker.model.dto.mapper.ProductMapper;
import com.broker.repository.AccountRepository;
import com.broker.repository.RecipeRepository;
import com.broker.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RecipeService {

    private final RecipeRepository recipeRepository;
    private final AccountRepository accountRepository;
    private final ProductRepository productRepository;

    public RecipeService(RecipeRepository recipeRepository,
                         AccountRepository accountRepository,
                         ProductRepository productRepository){
        this.recipeRepository = recipeRepository;
        this.accountRepository = accountRepository;
        this.productRepository = productRepository;
    }


    private void computeNutritionalValuesForRecipeAndSave(CreateRecipeDTO createRecipeDTO){
        Recipe recipe = new Recipe();

        List<Product> products = createRecipeDTO.getProductList().stream()
                .map(productDTO -> productRepository.findProductByName(productDTO.getName()))
                .collect(Collectors.toList());

        recipe.setAccount(accountRepository.findAccountByUid(createRecipeDTO.getAccountId()));
        recipe.setName(createRecipeDTO.getName());
        recipe.setDetails(createRecipeDTO.getDetails());
        recipe.setQuantity(createRecipeDTO.getProductList()
                .stream()
                .map(CreateProductDTO::getQuantity)
                .reduce(0, Integer::sum));

        Integer totalCalories;
        Double totalProtein, totalCarbs, totalFibers, totalFats;

        totalProtein = createRecipeDTO.getProductList()
                .stream().map(productDTO -> productDTO.getProtein() * productDTO.getQuantity() / 100)
                .reduce(0.0, Double::sum);

        totalCalories = createRecipeDTO.getProductList()
                .stream().map(productDTO -> productDTO.getCalories() * productDTO.getQuantity() / 100)
                .reduce(0, Integer::sum);

        totalCarbs = createRecipeDTO.getProductList()
                .stream().map(productDTO -> productDTO.getCarbs() * productDTO.getQuantity() / 100)
                .reduce(0.0, Double::sum);

        totalFibers = createRecipeDTO.getProductList()
                .stream().map(productDTO -> productDTO.getFibre() * productDTO.getQuantity() / 100)
                .reduce(0.0, Double::sum);

        totalFats = createRecipeDTO.getProductList()
                .stream().map(productDTO -> productDTO.getFat() * productDTO.getQuantity() / 100)
                .reduce(0.0, Double::sum);

        recipe.setProtein(totalProtein * 100 / recipe.getQuantity());
        recipe.setCarbs(totalCarbs * 100 / recipe.getQuantity());
        recipe.setCalories(totalCalories * 100 / recipe.getQuantity());
        recipe.setFibre(totalFibers * 100 / recipe.getQuantity());
        recipe.setFat(totalFats * 100 / recipe.getQuantity());

        recipe.setProducts(products);
        recipeRepository.saveAndFlush(recipe);
    }

    public CreateRecipeDTO saveCookedProduct(CreateRecipeDTO recipe){
        recipe.getProductList()
                .forEach(productDTO -> {
                    if(productRepository.findProductByName(productDTO.getName()) == null) {
                        productRepository.saveAndFlush(ProductMapper.fromCreateProductDTOToProduct(productDTO));
                    }
                });
        computeNutritionalValuesForRecipeAndSave(recipe);
        return recipe;
    }

    public List<RecipeDTO> getCookedProducts(Integer accountId){

        return recipeRepository
                .findRecipeByAccount_Uid(accountId)
                .stream()
                .map(CookedProductMapper::fromCookedProductToCookedProductDTO)
                .collect(Collectors.toList());
    }

    public RecipeDTO deleteRecipe(RecipeDTO recipeDTO){

        recipeRepository.deleteRecipeByNameAndAccount_Uid(recipeDTO.getName(), recipeDTO.getAccountId());

        return recipeDTO;
    }
}
