package com.broker.service;

import com.broker.model.NotificationProfile;
import com.broker.model.dto.NotificationProfileDTO;
import com.broker.model.dto.mapper.NotificationProfileMapper;
import com.broker.repository.AccountRepository;
import com.broker.repository.NotificationProfileRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class NotificationProfileService {

    private NotificationProfileRepository notificationProfileRepository;
    private AccountRepository accountRepository;

    public NotificationProfileService(NotificationProfileRepository notificationProfileRepository,
                                      AccountRepository accountRepository){
        this.notificationProfileRepository = notificationProfileRepository;
        this.accountRepository = accountRepository;
    }

    public NotificationProfileDTO getNotificationProfileForUser(Integer accountId){

        Optional<NotificationProfile> optionalNotificationProfileDTO = notificationProfileRepository.findNotificationProfileByNotificationUser_Uid(accountId);

        if(!optionalNotificationProfileDTO.isPresent()){
            return saveNotificationProfile(accountId);
        }

        return NotificationProfileMapper.fromNotificationProfileToNotificationProfileDTO(
                notificationProfileRepository.findNotificationProfileByNotificationUser_Uid(accountId).get());
    }

    public NotificationProfileDTO saveNotificationProfile(Integer accountId){
        NotificationProfile notificationProfile = new NotificationProfile();
        notificationProfile.setNotificationUser(accountRepository.findAccountByUid(accountId));
        notificationProfile.setProfileSettings("DEFAULT;CalorieIntake.14:00,19:30;WaterIntake.12:00,19:00,22:00;CalorieBurning.18:00,22:00");

        return NotificationProfileMapper.fromNotificationProfileToNotificationProfileDTO(
                notificationProfileRepository.saveAndFlush(notificationProfile));
    }

    public NotificationProfileDTO updateNotificationProfile(NotificationProfileDTO notificationProfileDTO){
        Optional<NotificationProfile> notificationProfileOptional = notificationProfileRepository.findNotificationProfileByNotificationUser_Uid(notificationProfileDTO.getAccountId());

        NotificationProfile notificationProfile = notificationProfileOptional.get();
        notificationProfile.setProfileSettings(notificationProfileDTO.getProfileSettings());

        notificationProfileRepository.flush();

        return NotificationProfileMapper.fromNotificationProfileToNotificationProfileDTO(notificationProfile);
    }
}
