package com.broker.service;

import com.broker.model.Account;
import com.broker.model.Sex;
import com.broker.model.User;
import com.broker.model.dto.AccountDTO;
import com.broker.model.dto.CreateAccountDTO;
import com.broker.model.dto.mapper.AccountMapper;
import com.broker.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AccountService {

    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AccountService(AccountRepository accountRepository,
                          PasswordEncoder passwordEncoder){
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public AccountDTO getAccountByAuthDetails(User user){

        Account account = accountRepository.findAccountByUsername(user.getUsername());

        if(passwordEncoder.matches(user.getPassword(), account.getPassword())){
            return AccountMapper.fromAccountToAccountDTO(account, computeAge(account.getDateOfBirth()));
        }

        return null;
    }

    public List<Integer> getAllAccountIds(){
        return accountRepository.findAll().stream().map(Account::getUid).collect(Collectors.toList());
    }

    private Integer computeAge(LocalDate dateOfBirth){
        Period period = Period.between(dateOfBirth, LocalDate.now());

        return Math.abs(period.getYears());
    }

    public AccountDTO saveAccount(CreateAccountDTO createAccountDTO){
        Account account = AccountMapper.fromCreateAccountDTOToAccount(createAccountDTO);
        Integer age = computeAge(account.getDateOfBirth());
        account.setRequiredCalories(computeCalories(account, age));
        account.setWaterIntake(computeNecessaryWaterIntake(account));
        account.setPassword(passwordEncoder.encode(account.getPassword()));

        return AccountMapper.fromAccountToAccountDTO(accountRepository.saveAndFlush(account), age);
    }

    public AccountDTO updateAccount(AccountDTO accountDTO){
        Account account = accountRepository.findAccountByUid(accountDTO.getUid());
        account.setUsername(accountDTO.getUsername());
        account.setEmail(accountDTO.getEmail());
        account.setWeight(accountDTO.getWeight());
        Integer age = computeAge(account.getDateOfBirth());
        account.setRequiredCalories(computeCalories(account, age));
        account.setWaterIntake(computeNecessaryWaterIntake(account));

        accountDTO.setCalories(account.getRequiredCalories());
        accountDTO.setWaterIntake(account.getWaterIntake());

        accountRepository.flush();

        return accountDTO;
    }

    private Integer computeNecessaryWaterIntake(Account account){
        int waterIntake = 0;
        waterIntake = account.getWeight() * 2/3;
        switch (account.getActivityLevel()){
            case LOW_ACTIVITY:
                waterIntake += 12;
                break;
            case MODERATE_ACTIVITY:
                waterIntake += 24;
                break;
            case ACTIVE:
                waterIntake += 36;
                break;
            case VERY_ACTIVE:
                waterIntake += 48;
                break;
            default:
                break;
        }
        return (int)(waterIntake * 29.75);
    }
    private Integer computeCalories(Account account, Integer age){
        double necessaryCalories = 0.0;
        if(account.getSex().equals(Sex.MALE)){
            necessaryCalories = 66.47 +
                    (13.75 * account.getWeight()) +
                    (5.003 * account.getHeight()) -
                    (6.755 * age);
        }else{
            necessaryCalories = 655.1 +
                    (9.563 * account.getWeight()) +
                    (1.850 * account.getHeight()) -
                    (4.676 * age);
        }
        switch (account.getActivityLevel()){
            case SEDENTARY:
                necessaryCalories *= 1.2;
                break;
            case LOW_ACTIVITY:
                necessaryCalories *= 1.375;
                break;
            case MODERATE_ACTIVITY:
                necessaryCalories *= 1.55;
                break;
            case ACTIVE:
                necessaryCalories *= 1.725;
                break;
            case VERY_ACTIVE:
                necessaryCalories *= 1.9;
                break;
            default:
                break;
        }

        return (int)necessaryCalories;
    }
}
