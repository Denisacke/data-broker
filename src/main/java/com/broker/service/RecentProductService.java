package com.broker.service;

import com.broker.model.Account;
import com.broker.model.Recipe;
import com.broker.model.Product;
import com.broker.model.RecentProduct;
import com.broker.model.dto.CreateProductDTO;
import com.broker.model.dto.MacroProductDTO;
import com.broker.model.dto.RecentProductDTO;
import com.broker.model.dto.mapper.CookedProductMapper;
import com.broker.model.dto.mapper.ProductMapper;
import com.broker.model.dto.mapper.RecentProductMapper;
import com.broker.repository.AccountRepository;
import com.broker.repository.RecipeRepository;
import com.broker.repository.ProductRepository;
import com.broker.repository.RecentProductRepository;
import org.assertj.core.util.Lists;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RecentProductService {

    private final RecentProductRepository recentProductRepository;
    private final ProductRepository productRepository;
    private final AccountRepository accountRepository;
    private final RecipeRepository recipeRepository;

    public RecentProductService(RecentProductRepository recentProductRepository,
                                ProductRepository productRepository,
                                AccountRepository accountRepository,
                                RecipeRepository recipeRepository){
        this.recentProductRepository = recentProductRepository;
        this.productRepository = productRepository;
        this.accountRepository = accountRepository;
        this.recipeRepository = recipeRepository;
    }

    public RecentProductDTO addRecentlyConsumedProduct(RecentProductDTO recentProduct){
        recentProduct.setDate(LocalDate.now());
        RecentProduct recentProductEntry = new RecentProduct();
        Account account = accountRepository.getOne(recentProduct.getUserId());
        Product product = null;
        Recipe recipe = null;
        if(recentProduct.getIsCooked()){
            recipe = recipeRepository.findRecipeByName(recentProduct.getProductName());
            recentProductEntry = RecentProductMapper
                    .fromRecentProductDTOToRecentProduct(
                            recentProduct,
                            null,
                            recipe,
                            account);
            System.out.println("Adding recent consumption of recipe");
            List<RecentProduct> recentProductList;
            try{
                recentProductList = recentProductRepository.findAllByRecipe(recipe);
                System.out.println("Found recent records of consumption");
            }catch (Exception e){
                recentProductList = new ArrayList<>();
                System.out.println("Allocated for new list");
            }

            recentProductList.add(recentProductEntry);
            recipe.setRecentProducts(recentProductList);
            System.out.println("Got before flush");
            recipeRepository.saveAndFlush(recipe);
        }else{
            System.out.println("Got to updating product");
            product = productRepository.findProductByName(recentProduct.getProductName());

            recentProductEntry = RecentProductMapper
                    .fromRecentProductDTOToRecentProduct(
                            recentProduct,
                            product,
                            null,
                            account);
            System.out.println("Adding recent consumption");
            List<RecentProduct> recentProductList;
            try{
                recentProductList = recentProductRepository.findAllByProduct(product);
                System.out.println("Found recent records of consumption");
            }catch (Exception e){
                recentProductList = new ArrayList<>();
                System.out.println("Allocated for new list");
            }

            recentProductList.add(recentProductEntry);
            product.setRecentProducts(recentProductList);
            System.out.println("Got before flush");
            productRepository.saveAndFlush(product);
        }
        System.out.println("User id is: " + recentProduct.getUserId());

        return recentProduct;
//        return recentProductRepository.save(RecentProductMapper.fromRecentProductDTOToRecentProduct(recentProduct, product, cookedProduct, account));
    }

    public Pair<Integer, Integer> getFullConsumptionForUser(Integer accountId){
        List<RecentProductDTO> recentProductList = recentProductRepository
                .findAll()
                .stream()
                .map(RecentProductMapper::fromRecentProductToRecentProductDTO)
                .filter(recentProductDTO -> Objects.equals(recentProductDTO.getUserId(), accountId))
                .collect(Collectors.toList());

        int calorieConsumption;
        int waterIntake;

        try{
            calorieConsumption = recentProductList.stream()
                    .filter(recentProduct -> !recentProduct.getProductName().equals("Apa"))
                    .mapToInt(recentProduct -> recentProduct.getQuantity() * productRepository.findProductByName(recentProduct.getProductName()).getCalories() / 100)
                    .sum();

            waterIntake = recentProductList.stream()
                    .filter(recentProduct -> recentProduct.getProductName().equals("Apa"))
                    .mapToInt(RecentProductDTO::getQuantity)
                    .sum();
        }catch (Exception e){
            calorieConsumption = 0;
            waterIntake = 0;
        }

        return Pair.of(calorieConsumption, waterIntake);
    }

    public List<CreateProductDTO> getRecentlyConsumedProductsForUser(Integer accountId){
        List<RecentProductDTO> recentProductList = recentProductRepository
                .findAll()
                .stream()
                .map(RecentProductMapper::fromRecentProductToRecentProductDTO)
                .filter(recentProductDTO -> Objects.equals(recentProductDTO.getUserId(), accountId))
                .sorted(Comparator.comparing(RecentProductDTO::getDate))
                .collect(Collectors.toList());

        Set<CreateProductDTO> createProductDTOList = recentProductList
                .stream()
                .filter(recentProductDTO -> recipeRepository.findRecipeByName(recentProductDTO.getProductName()) == null)
                .map(recentProductDTO -> productRepository.findProductByName(recentProductDTO.getProductName()))
                .filter(product -> !product.getName().equals("Apa"))
                .map(ProductMapper::fromProductToCreateProductDTO)
                .collect(Collectors.toSet());
        return Lists.newArrayList(createProductDTOList);
    }
    public MacroProductDTO getDailyMacrosForUser(Integer accountId){
        List<RecentProductDTO> recentProductList = recentProductRepository
                .findAllByDate(LocalDate.now())
                .stream()
                .map(RecentProductMapper::fromRecentProductToRecentProductDTO)
                .filter(recentProductDTO -> Objects.equals(recentProductDTO.getUserId(), accountId))
                .collect(Collectors.toList());

        double protein, fat, carbs;

        try{
            protein = recentProductList.stream()
                    .filter(recentProduct -> !recentProduct.getProductName().equals("Apa"))
                    .mapToDouble(recentProduct -> {
                        if(productRepository.findProductByName(recentProduct.getProductName()) == null){
                            return recentProduct.getQuantity() * recipeRepository.findRecipeByName(recentProduct.getProductName()).getProtein() / 100;
                        }
                        return recentProduct.getQuantity() * productRepository.findProductByName(recentProduct.getProductName()).getProtein() / 100;
                    })
                    .sum();

            fat = recentProductList.stream()
                    .filter(recentProduct -> !recentProduct.getProductName().equals("Apa"))
                    .mapToDouble(recentProduct -> {
                        if(productRepository.findProductByName(recentProduct.getProductName()) == null) {
                            return recentProduct.getQuantity() * recipeRepository.findRecipeByName(recentProduct.getProductName()).getFat() / 100;
                        }
                        return recentProduct.getQuantity() * productRepository.findProductByName(recentProduct.getProductName()).getFat() / 100;
                    })
                    .sum();

            carbs = recentProductList.stream()
                    .filter(recentProduct -> !recentProduct.getProductName().equals("Apa"))
                    .mapToDouble(recentProduct -> {
                        if(productRepository.findProductByName(recentProduct.getProductName()) == null) {
                            return recentProduct.getQuantity() * recipeRepository.findRecipeByName(recentProduct.getProductName()).getCarbs() / 100;
                        }
                        return recentProduct.getQuantity() * productRepository.findProductByName(recentProduct.getProductName()).getCarbs() / 100;
                    })
                    .sum();
        }catch (Exception e){
            protein = 0;
            fat = 0;
            carbs = 0;
        }

        MacroProductDTO macroProductDTO = new MacroProductDTO();
        macroProductDTO.setFat(fat);
        macroProductDTO.setProtein(protein);
        macroProductDTO.setCarbs(carbs);

        return macroProductDTO;
    }
    public Pair<Integer, Integer> getTodayConsumptionForUser(Integer accountId){
        List<RecentProductDTO> recentProductList = recentProductRepository
                .findAllByDate(LocalDate.now())
                .stream()
                .map(RecentProductMapper::fromRecentProductToRecentProductDTO)
                .filter(recentProductDTO -> Objects.equals(recentProductDTO.getUserId(), accountId))
                .collect(Collectors.toList());

        int calorieConsumption;
        int waterIntake;

        try{
            calorieConsumption = recentProductList.stream()
                    .filter(recentProduct -> !recentProduct.getProductName().equals("Apa"))
                    .mapToInt(recentProduct -> {
                        if(productRepository.findProductByName(recentProduct.getProductName()) == null){
                            return recentProduct.getQuantity() * recipeRepository.findRecipeByName(recentProduct.getProductName()).getCalories() / 100;
                        }
                        return recentProduct.getQuantity() * productRepository.findProductByName(recentProduct.getProductName()).getCalories() / 100;
                    })
                    .sum();

            waterIntake = recentProductList.stream()
                    .filter(recentProduct -> recentProduct.getProductName().equals("Apa"))
                    .mapToInt(RecentProductDTO::getQuantity)
                    .sum();
        }catch (Exception e){
            calorieConsumption = 0;
            waterIntake = 0;
        }

        return Pair.of(calorieConsumption, waterIntake);
    }

}
