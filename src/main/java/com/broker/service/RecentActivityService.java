package com.broker.service;

import com.broker.model.Account;
import com.broker.model.Activity;
import com.broker.model.RecentActivity;
import com.broker.model.dto.RecentActivityDTO;
import com.broker.model.dto.mapper.RecentActivityMapper;
import com.broker.repository.AccountRepository;
import com.broker.repository.ActivityRepository;
import com.broker.repository.RecentActivityRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RecentActivityService {

    private RecentActivityRepository recentActivityRepository;
    private AccountRepository accountRepository;
    private ActivityRepository activityRepository;

    public RecentActivityService(RecentActivityRepository recentActivityRepository,
                                 AccountRepository accountRepository,
                                 ActivityRepository activityRepository){
        this.recentActivityRepository = recentActivityRepository;
        this.accountRepository = accountRepository;
        this.activityRepository = activityRepository;
    }

    public RecentActivityDTO saveRecentActivity(RecentActivityDTO recentActivityDTO){
        recentActivityDTO.setDate(LocalDate.now());
        Account account = accountRepository.findAccountByUid(recentActivityDTO.getAccountId());
        Activity activity = activityRepository.findActivityByUid(recentActivityDTO.getActivityId());

        recentActivityRepository.saveAndFlush(RecentActivityMapper
                        .fromRecentActivityDTOToRecentActivity(recentActivityDTO, activity, account));

        return recentActivityDTO;
    }

    public List<RecentActivityDTO> getRecentActivitiesForUser(Integer accountId){

        List<RecentActivityDTO> recentActivityDTOS = recentActivityRepository
                .findRecentActivitiesByActiveUser_Uid(accountId)
                .stream()
                .filter(recentActivity -> Objects.equals(recentActivity.getDate(), LocalDate.now()))
                .map(RecentActivityMapper::fromRecentActivityToRecentActivityDTO)
                .collect(Collectors.toList());

        return recentActivityDTOS;
    }

    public Integer getTodayCaloriesBurnedForUser(Integer accountId){

        Optional<Integer> caloriesBurned = recentActivityRepository
                .findRecentActivitiesByActiveUser_Uid(accountId)
                .stream()
                .filter(recentActivity -> Objects.equals(recentActivity.getDate(), LocalDate.now()))
                .map(RecentActivity::getBurnedCalories)
                .reduce(Integer::sum);

        return caloriesBurned.orElse(0);
    }
}
