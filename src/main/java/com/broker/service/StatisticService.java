package com.broker.service;

import com.broker.model.Account;
import com.broker.model.Statistic;
import com.broker.model.dto.GraphStatisticDTO;
import com.broker.model.dto.StatisticDTO;
import com.broker.model.dto.mapper.StatisticMapper;
import com.broker.repository.AccountRepository;
import com.broker.repository.RecentActivityRepository;
import com.broker.repository.StatisticRepository;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StatisticService {

    private final StatisticRepository statisticRepository;
    private final AccountRepository accountRepository;

    public StatisticService(StatisticRepository statisticRepository,
                            AccountRepository accountRepository){
        this.statisticRepository = statisticRepository;
        this.accountRepository = accountRepository;
    }

    public void saveStatistics(Pair<Integer, Integer> consumption, Integer caloriesBurned, Integer accountId){
        Optional<Statistic> statisticOptional = statisticRepository
                .findTopStatisticByDateAndUserStatistic_Uid(LocalDate.now(), accountId);
        Statistic statistic = new Statistic();
        if(statisticOptional.isPresent()){
            Account account = accountRepository.findAccountByUid(accountId);

            statistic = statisticOptional.get();
            statistic.setUserStatistic(account);

            statistic.setCalorieIntake(consumption.getFirst());
            statistic.setWaterIntake(consumption.getSecond());
            statistic.setCaloriesBurned(caloriesBurned);

//            statisticRepository.flush();
            statisticRepository.saveAndFlush(statistic);
            return;
        }

        statistic.setDate(LocalDate.now());
        statistic.setCalorieIntake(consumption.getFirst());
        statistic.setWaterIntake(consumption.getSecond());
        statistic.setCaloriesBurned(caloriesBurned);
        statistic.setUserStatistic(accountRepository.findAccountByUid(accountId));

        statisticRepository.saveAndFlush(statistic);
    }

    public List<StatisticDTO> getAllStatistics(){
        return statisticRepository.findAll().stream().map(StatisticMapper::fromStatisticToStatisticDTO).collect(Collectors.toList());
    }
    public List<GraphStatisticDTO> getStatisticsForUser(Integer accountId){

        return statisticRepository.findAllByUserStatistic_Uid(accountId).stream().map(StatisticMapper::fromStatisticToGraphStatisticDTO).collect(Collectors.toList());
    }
}
