package com.broker.service;

import com.broker.model.Activity;
import com.broker.model.dto.ActivityDTO;
import com.broker.model.dto.RecentActivityDTO;
import com.broker.model.dto.mapper.ActivityMapper;
import com.broker.repository.ActivityRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ActivityService {

    private ActivityRepository activityRepository;

    public ActivityService(ActivityRepository activityRepository){
        this.activityRepository = activityRepository;
    }

    public List<Activity> getAllActivities(){
        return activityRepository.findAll();
    }

    public List<ActivityDTO> getAllActivitiesByCategory(String category){

        return activityRepository
                .findAllByCategory(category)
                .stream()
                .map(ActivityMapper::fromActivityToActivityDTO)
                .collect(Collectors.toList());
    }
    public void saveActivityList(List<Activity> activityList){
        activityList.forEach(activity -> activityRepository.saveAndFlush(activity));
    }
    public List<ActivityDTO> getActivitiesByName(String activityName){
        return activityRepository.findAllByNameContaining(activityName)
                .stream()
                .map(ActivityMapper::fromActivityToActivityDTO)
                .collect(Collectors.toList());
    }

}
