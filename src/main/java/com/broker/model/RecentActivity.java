package com.broker.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
public class RecentActivity {

    @Id
    @Column(name = "recent_activity_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int uid;

    @Column(name = "duration", nullable = false)
    private int duration;

    @Column(name = "burnedCalories", nullable = false)
    private int burnedCalories;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account activeUser;

    @ManyToOne
    @JoinColumn(name = "activity_id", nullable = false)
    private Activity finishedActivity;

    public RecentActivity(){

    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Account getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(Account activeUser) {
        this.activeUser = activeUser;
    }

    public Activity getFinishedActivity() {
        return finishedActivity;
    }

    public void setFinishedActivity(Activity finishedActivity) {
        this.finishedActivity = finishedActivity;
    }

    public int getBurnedCalories() {
        return burnedCalories;
    }

    public void setBurnedCalories(int burnedCalories) {
        this.burnedCalories = burnedCalories;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "RecentActivity{" +
                "uid=" + uid +
                ", duration=" + duration +
                ", burnedCalories=" + burnedCalories +
                ", activeUser=" + activeUser +
                '}';
    }
}
