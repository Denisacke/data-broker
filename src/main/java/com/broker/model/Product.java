package com.broker.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Product {

    @Id
    @Column(name = "product_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int uid;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "calories", nullable = false)
    private int calories;

    @Column(name = "protein", nullable = false)
    private double protein;

    @Column(name = "fibre", nullable = false)
    private double fibre;

    @Column(name = "carbs", nullable = false)
    private double carbs;

    @Column(name = "fat", nullable = false)
    private double fat;

    @Column(name = "details")
    private String details;

    @ManyToMany(mappedBy = "products", cascade = CascadeType.ALL)
    private List<Recipe> recipes;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<RecentProduct> recentProducts;

    public Product(){

    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getFibre() {
        return fibre;
    }

    public void setFibre(double fibre) {
        this.fibre = fibre;
    }

    public double getCarbs() {
        return carbs;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public void addCookedProduct(Recipe recipe){
        if(this.recipes == null){
            this.recipes = new ArrayList<>();
        }
        this.recipes.add(recipe);
    }

    public List<Recipe> getCookedProducts() {
        return recipes;
    }

    public void setCookedProducts(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    public List<RecentProduct> getRecentProducts() {
        return recentProducts;
    }

    public void setRecentProducts(List<RecentProduct> recentProducts) {
        this.recentProducts = recentProducts;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", calories=" + calories +
                ", protein=" + protein +
                ", fibre=" + fibre +
                ", carbs=" + carbs +
                ", fat=" + fat +
                ", details='" + details + '\'' +
                ", cookedProducts=" + recipes +
                '}';
    }
}
