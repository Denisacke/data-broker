package com.broker.model.dto;

public class NotificationProfileDTO {

    private int uid;

    private String profileSettings;

    private Integer accountId;

    public NotificationProfileDTO(){

    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getProfileSettings() {
        return profileSettings;
    }

    public void setProfileSettings(String profileSettings) {
        this.profileSettings = profileSettings;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return "NotificationProfileDTO{" +
                "uid=" + uid +
                ", profileSettings='" + profileSettings + '\'' +
                ", accountId=" + accountId +
                '}';
    }
}
