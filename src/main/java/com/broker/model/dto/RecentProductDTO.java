package com.broker.model.dto;

import java.time.LocalDate;

public class RecentProductDTO {

    private int uid;

    private String productName;

    private Integer userId;

    private int quantity;

    private LocalDate date;

    private boolean isCooked;

    public RecentProductDTO(){

    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean getIsCooked() {
        return isCooked;
    }

    public void setIsCooked(boolean cooked) {
        isCooked = cooked;
    }

    @Override
    public String toString() {
        return "RecentProductDTO{" +
                "productName='" + productName + '\'' +
                ", userId=" + userId +
                ", quantity=" + quantity +
                ", date=" + date +
                ", isCooked=" + isCooked +
                '}';
    }
}
