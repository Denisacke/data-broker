package com.broker.model.dto;

import java.time.LocalDate;

public class RecentActivityDTO {

    private Integer duration;

    private Integer accountId;

    private String activityName;

    private Integer burnedCalories;

    private Integer activityId;

    private LocalDate date;

    public RecentActivityDTO(){

    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public int getBurnedCalories() {
        return burnedCalories;
    }

    public void setBurnedCalories(int burnedCalories) {
        this.burnedCalories = burnedCalories;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "RecentActivityDTO{" +
                "duration=" + duration +
                ", accountId=" + accountId +
                ", activityName='" + activityName + '\'' +
                ", burnedCalories=" + burnedCalories +
                ", activityId=" + activityId +
                ", date=" + date +
                '}';
    }
}
