package com.broker.model.dto.mapper;

import com.broker.model.Account;
import com.broker.model.Activity;
import com.broker.model.RecentActivity;
import com.broker.model.dto.RecentActivityDTO;

public class RecentActivityMapper {

    public static RecentActivity fromRecentActivityDTOToRecentActivity(RecentActivityDTO recentActivityDTO,
                                                                       Activity activity,
                                                                       Account account){
        RecentActivity recentActivity = new RecentActivity();
        recentActivity.setFinishedActivity(activity);
        recentActivity.setActiveUser(account);
        recentActivity.setBurnedCalories(recentActivityDTO.getBurnedCalories());
        recentActivity.setDuration(recentActivityDTO.getDuration());
        recentActivity.setDate(recentActivityDTO.getDate());

        return recentActivity;
    }

    public static RecentActivityDTO fromRecentActivityToRecentActivityDTO(RecentActivity activity){

        RecentActivityDTO recentActivityDTO = new RecentActivityDTO();
        recentActivityDTO.setActivityName(activity.getFinishedActivity().getName());
        recentActivityDTO.setAccountId(activity.getActiveUser().getUid());
        recentActivityDTO.setActivityId(activity.getFinishedActivity().getUid());
        recentActivityDTO.setDuration(activity.getDuration());
        recentActivityDTO.setBurnedCalories(activity.getBurnedCalories());
        recentActivityDTO.setDate(activity.getDate());

        return recentActivityDTO;
    }
}
