package com.broker.model.dto.mapper;

import com.broker.model.Account;
import com.broker.model.ActivityLevel;
import com.broker.model.Sex;
import com.broker.model.dto.AccountDTO;
import com.broker.model.dto.CreateAccountDTO;

import java.time.LocalDate;

public class AccountMapper {

    public static Account fromCreateAccountDTOToAccount(CreateAccountDTO createAccountDTO){

        Account account = new Account();
        account.setPassword(createAccountDTO.getPassword());
        account.setUid(createAccountDTO.getUid());
        account.setUsername(createAccountDTO.getUsername());
        account.setDateOfBirth(LocalDate.parse(createAccountDTO.getDateOfBirth()));
        account.setHeight(createAccountDTO.getHeight());
        account.setWeight(createAccountDTO.getWeight());
        account.setEmail(createAccountDTO.getEmail());
        account.setSex(Sex.valueOf(createAccountDTO.getSex()));
        account.setActivityLevel(ActivityLevel.valueOf(createAccountDTO.getActivityLevel()));

        return account;
    }

    public static AccountDTO fromAccountToAccountDTO(Account account, Integer age){

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setUid(account.getUid());
        accountDTO.setAge(age);
        accountDTO.setCalories(account.getRequiredCalories());
        accountDTO.setEmail(account.getEmail());
        accountDTO.setHeight(account.getHeight());
        accountDTO.setWeight(account.getWeight());
        accountDTO.setUsername(account.getUsername());
        accountDTO.setWaterIntake(account.getWaterIntake());

        return accountDTO;
    }
}
