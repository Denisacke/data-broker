package com.broker.model.dto.mapper;

import com.broker.model.Account;
import com.broker.model.Statistic;
import com.broker.model.dto.GraphStatisticDTO;
import com.broker.model.dto.StatisticDTO;

import java.time.format.DateTimeFormatter;

public class StatisticMapper {

    public static Statistic fromStatisticDTOToStatistic(StatisticDTO statisticDTO, Account account){
        Statistic statistic = new Statistic();

        statistic.setCaloriesBurned(statisticDTO.getCaloriesBurned());
        statistic.setDate(statisticDTO.getDate());
        statistic.setWaterIntake(statisticDTO.getWaterIntake());
        statistic.setCalorieIntake(statisticDTO.getCalorieIntake());
        statistic.setUserStatistic(account);
        statistic.setUid(statisticDTO.getUid());

        return statistic;
    }

    public static GraphStatisticDTO fromStatisticToGraphStatisticDTO(Statistic statistic){
        GraphStatisticDTO statisticDTO = new GraphStatisticDTO();

        statisticDTO.setCalorieIntake(statistic.getCalorieIntake());
        statisticDTO.setDate(statistic.getDate().format(DateTimeFormatter.ofPattern("yyyy.MM.dd")));
        statisticDTO.setCaloriesBurned(statistic.getCaloriesBurned());
        statisticDTO.setUid(statistic.getUid());
        statisticDTO.setAccountId(statistic.getUserStatistic().getUid());
        statisticDTO.setWaterIntake(statistic.getWaterIntake());

        return statisticDTO;
    }
    public static StatisticDTO fromStatisticToStatisticDTO(Statistic statistic){
        StatisticDTO statisticDTO = new StatisticDTO();

        statisticDTO.setCalorieIntake(statistic.getCalorieIntake());
        statisticDTO.setDate(statistic.getDate());
        statisticDTO.setCaloriesBurned(statistic.getCaloriesBurned());
        statisticDTO.setUid(statistic.getUid());
        statisticDTO.setAccountId(statistic.getUserStatistic().getUid());
        statisticDTO.setWaterIntake(statistic.getWaterIntake());

        return statisticDTO;
    }
}
