package com.broker.model.dto.mapper;

import com.broker.model.Recipe;
import com.broker.model.Product;
import com.broker.model.RecentProduct;
import com.broker.model.dto.CreateProductDTO;
import com.broker.model.dto.MacroProductDTO;
import com.broker.model.dto.ProductDTO;

import java.util.stream.Collectors;

public class ProductMapper {

    public static Product fromCreateProductDTOToProduct(CreateProductDTO productDTO){
        Product product = new Product();

        product.setCalories(productDTO.getCalories());
        product.setCarbs(productDTO.getCarbs());
        product.setFat(productDTO.getFat());
        product.setName(productDTO.getName());
        product.setFibre(productDTO.getFibre());
        product.setProtein(productDTO.getProtein());
        product.setDetails(productDTO.getDetails());

        return product;
    }

    public static CreateProductDTO fromProductToCreateProductDTO(Product product){
        CreateProductDTO createProductDTO = new CreateProductDTO();

        createProductDTO.setName(product.getName());
        createProductDTO.setCalories(product.getCalories());
        createProductDTO.setCarbs(product.getCarbs());
        createProductDTO.setProtein(product.getProtein());
        createProductDTO.setFat(product.getFat());
        createProductDTO.setFibre(product.getFibre());
        createProductDTO.setUid(product.getUid());

        return createProductDTO;
    }

    public static MacroProductDTO fromProductToMacroProduct(Product product){
        MacroProductDTO macroProductDTO = new MacroProductDTO();
        macroProductDTO.setCarbs(product.getCarbs());
        macroProductDTO.setProtein(product.getProtein());
        macroProductDTO.setFat(product.getFat());

        return macroProductDTO;
    }
    public static Product fromProductDTOToProduct(ProductDTO productDTO){
        Product product = new Product();

        product.setCalories(productDTO.getCalories());
        product.setCarbs(productDTO.getCarbs());
        product.setFat(productDTO.getFat());
        product.setName(productDTO.getName());
        product.setFibre(productDTO.getFibre());
        product.setProtein(productDTO.getProtein());
        product.setDetails(productDTO.getDetails());

        return product;
    }
    public static ProductDTO fromProductToProductDTO(Product product){
        ProductDTO productDTO = new ProductDTO();

        productDTO.setUid(product.getUid());
        productDTO.setName(product.getName());
        productDTO.setFat(product.getFat());
        productDTO.setFibre(product.getFibre());
        productDTO.setProtein(product.getProtein());
        productDTO.setDetails(product.getDetails());
        productDTO.setCarbs(product.getCarbs());
        productDTO.setCalories(product.getCalories());

        if(product.getCookedProducts() != null){
            productDTO.setCookedProducts(product.getCookedProducts()
                    .stream()
                    .map(Recipe::getUid)
                    .collect(Collectors.toList()));
        }

        if(product.getRecentProducts() != null){
            productDTO.setRecentProducts(product.getRecentProducts()
                    .stream()
                    .map(RecentProduct::getUid)
                    .collect(Collectors.toList()));
        }

        return productDTO;
    }
}
