package com.broker.model.dto.mapper;

import com.broker.model.Account;
import com.broker.model.Recipe;
import com.broker.model.Product;
import com.broker.model.dto.RecipeDTO;

import java.util.List;
import java.util.stream.Collectors;

public class CookedProductMapper {

    public static Recipe fromCookedProductDTOToCookedProduct(RecipeDTO recipeDTO,
                                                             List<Product> productList,
                                                             Account account){

        Recipe product = new Recipe();
        product.setAccount(account);
        product.setProducts(productList);
        product.setCalories(recipeDTO.getCalories());
        product.setCarbs(recipeDTO.getCarbs());
        product.setFat(recipeDTO.getFat());
        product.setFibre(recipeDTO.getFibre());
        product.setName(recipeDTO.getName());
        product.setDetails(recipeDTO.getDetails());
        product.setProtein(recipeDTO.getProtein());

        return product;
    }

    public static RecipeDTO fromCookedProductToCookedProductDTO(Recipe recipe){
        RecipeDTO productDTO = new RecipeDTO();
        productDTO.setCalories(recipe.getCalories());
        productDTO.setCarbs(recipe.getCarbs());
        productDTO.setDetails(recipe.getDetails());
        productDTO.setProtein(recipe.getProtein());
        productDTO.setName(recipe.getName());
        productDTO.setFat(recipe.getFat());
        productDTO.setFibre(recipe.getFibre());
        productDTO.setProductList(recipe.getProducts().stream().map(Product::getName).collect(Collectors.toList()));
        productDTO.setAccountId(recipe.getAccount().getUid());

        return productDTO;
    }
}
