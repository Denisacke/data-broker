package com.broker.model.dto.mapper;

import com.broker.model.Activity;
import com.broker.model.dto.ActivityDTO;

public class ActivityMapper {

    public static ActivityDTO fromActivityToActivityDTO(Activity activity){
        ActivityDTO activityDTO = new ActivityDTO();
        activityDTO.setMetCoefficient(activity.getMetCoefficient());
        activityDTO.setName(activity.getName());
        activityDTO.setUid(activity.getUid());
        activityDTO.setCategory(activity.getCategory());

        return activityDTO;
    }
}
