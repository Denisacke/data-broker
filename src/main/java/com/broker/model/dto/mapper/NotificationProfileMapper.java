package com.broker.model.dto.mapper;

import com.broker.model.NotificationProfile;
import com.broker.model.dto.NotificationProfileDTO;

public class NotificationProfileMapper {

    public static NotificationProfileDTO fromNotificationProfileToNotificationProfileDTO(NotificationProfile notificationProfile){
        NotificationProfileDTO notificationProfileDTO = new NotificationProfileDTO();

        notificationProfileDTO.setProfileSettings(notificationProfile.getProfileSettings());
        notificationProfileDTO.setUid(notificationProfile.getUid());
        notificationProfileDTO.setAccountId(notificationProfile.getNotificationUser().getUid());

        return notificationProfileDTO;
    }
}
