package com.broker.model.dto.mapper;

import com.broker.model.Account;
import com.broker.model.Recipe;
import com.broker.model.Product;
import com.broker.model.RecentProduct;
import com.broker.model.dto.RecentProductDTO;


public class RecentProductMapper {

    public static RecentProduct fromRecentProductDTOToRecentProduct(RecentProductDTO recentProductDTO,
                                                                    Product product,
                                                                    Recipe recipe,
                                                                    Account account){
        RecentProduct recentProduct = new RecentProduct();
        recentProduct.setProduct(product);
        recentProduct.setCookedProduct(recipe);
        recentProduct.setUser(account);
        recentProduct.setDate(recentProductDTO.getDate());
        recentProduct.setQuantity(recentProductDTO.getQuantity());
        recentProduct.setUid(recentProductDTO.getUid());

        return recentProduct;
    }

    public static RecentProductDTO fromRecentProductToRecentProductDTO(RecentProduct recentProduct){

        RecentProductDTO recentProductDTO = new RecentProductDTO();
        if(recentProduct.getProduct() != null){
            recentProductDTO.setProductName(recentProduct.getProduct().getName());
        }else if(recentProduct.getCookedProduct() != null){
            recentProductDTO.setProductName(recentProduct.getCookedProduct().getName());
        }
        recentProductDTO.setDate(recentProduct.getDate());
        recentProductDTO.setUid(recentProduct.getUid());
        recentProductDTO.setUserId(recentProduct.getUser().getUid());
        recentProductDTO.setQuantity(recentProduct.getQuantity());

        return recentProductDTO;
    }
}
