package com.broker.model.dto;

import java.util.List;

public class CreateRecipeDTO {

    private String name;

    private String details;

    private List<CreateProductDTO> productList;

    private Integer accountId;

    public CreateRecipeDTO(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<CreateProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<CreateProductDTO> productList) {
        this.productList = productList;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }
}
