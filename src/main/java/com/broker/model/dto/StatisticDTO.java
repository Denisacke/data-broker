package com.broker.model.dto;

import java.time.LocalDate;

public class StatisticDTO {

    private int uid;

    private LocalDate date;

    private int waterIntake;

    private int calorieIntake;

    private int caloriesBurned;

    private Integer accountId;

    public StatisticDTO(){

    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getWaterIntake() {
        return waterIntake;
    }

    public void setWaterIntake(int waterIntake) {
        this.waterIntake = waterIntake;
    }

    public int getCalorieIntake() {
        return calorieIntake;
    }

    public void setCalorieIntake(int calorieIntake) {
        this.calorieIntake = calorieIntake;
    }

    public int getCaloriesBurned() {
        return caloriesBurned;
    }

    public void setCaloriesBurned(int caloriesBurned) {
        this.caloriesBurned = caloriesBurned;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return "StatisticDTO{" +
                "uid=" + uid +
                ", date=" + date +
                ", waterIntake=" + waterIntake +
                ", calorieIntake=" + calorieIntake +
                ", caloriesBurned=" + caloriesBurned +
                ", accountId=" + accountId +
                '}';
    }
}
