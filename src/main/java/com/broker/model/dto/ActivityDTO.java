package com.broker.model.dto;

public class ActivityDTO {

    private Integer uid;

    private String name;

    private Double metCoefficient;

    private String category;

    public ActivityDTO(){

    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMetCoefficient() {
        return metCoefficient;
    }

    public void setMetCoefficient(Double metCoefficient) {
        this.metCoefficient = metCoefficient;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
