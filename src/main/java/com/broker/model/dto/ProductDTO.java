package com.broker.model.dto;

import java.util.List;

public class ProductDTO {

    private int uid;

    private String name;

    private int calories;

    private double protein;

    private double fibre;

    private double carbs;

    private double fat;

    private String details;

    private List<Integer> cookedProducts;

    private List<Integer> recentProducts;

    public ProductDTO(){

    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getFibre() {
        return fibre;
    }

    public void setFibre(double fibre) {
        this.fibre = fibre;
    }

    public double getCarbs() {
        return carbs;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<Integer> getCookedProducts() {
        return cookedProducts;
    }

    public void setCookedProducts(List<Integer> cookedProducts) {
        this.cookedProducts = cookedProducts;
    }

    public List<Integer> getRecentProducts() {
        return recentProducts;
    }

    public void setRecentProducts(List<Integer> recentProducts) {
        this.recentProducts = recentProducts;
    }
}
