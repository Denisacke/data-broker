package com.broker.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Recipe {

    @Id
    @Column(name = "recipe_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int uid;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "calories", nullable = false)
    private int calories;

    @Column(name = "protein", nullable = false)
    private double protein;

    @Column(name = "fibre", nullable = false)
    private double fibre;

    @Column(name = "carbs", nullable = false)
    private double carbs;

    @Column(name = "fat", nullable = false)
    private double fat;

    @Column(name = "details")
    private String details;

    @Column(name = "quantity")
    private int quantity;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "recipe_to_product",
            joinColumns = { @JoinColumn(name = "recipe_id") },
            inverseJoinColumns = { @JoinColumn(name = "product_id") }
    )
    private List<Product> products;

    @ManyToOne
    @JoinColumn(name="account_id", nullable=false)
    private Account account;

    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL)
    private List<RecentProduct> recentProducts;

    public Recipe(){

    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getFibre() {
        return fibre;
    }

    public void setFibre(double fibre) {
        this.fibre = fibre;
    }

    public double getCarbs() {
        return carbs;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<RecentProduct> getRecentProducts() {
        return recentProducts;
    }

    public void setRecentProducts(List<RecentProduct> recentProducts) {
        this.recentProducts = recentProducts;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "CookedProduct{" +
                "uid=" + uid +
                ", name='" + name + '\'' +
                ", calories=" + calories +
                ", protein=" + protein +
                ", fibre=" + fibre +
                ", carbs=" + carbs +
                ", fat=" + fat +
                ", details='" + details + '\'' +
                ", quantity=" + quantity +
                ", products=" + products +
                ", account=" + account +
                '}';
    }
}
