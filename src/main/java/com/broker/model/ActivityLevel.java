package com.broker.model;

public enum ActivityLevel {

    SEDENTARY,
    LOW_ACTIVITY,
    MODERATE_ACTIVITY,
    ACTIVE,
    VERY_ACTIVE
}
