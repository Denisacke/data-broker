package com.broker.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Activity{

    @Id
    @Column(name = "activity_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer uid;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "coefficient", nullable = false)
    private Double metCoefficient;

    @Column(name = "category", nullable = false)
    private String category;

    @OneToMany(mappedBy = "finishedActivity")
    private List<RecentActivity> recentActivities;

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMetCoefficient() {
        return metCoefficient;
    }

    public void setMetCoefficient(Double metCoefficient) {
        this.metCoefficient = metCoefficient;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<RecentActivity> getRecentActivities() {
        return recentActivities;
    }

    public void setRecentActivities(List<RecentActivity> recentActivities) {
        this.recentActivities = recentActivities;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "uid=" + uid +
                ", name='" + name + '\'' +
                ", metCoefficient=" + metCoefficient +
                ", category='" + category + '\'' +
                '}';
    }
}
