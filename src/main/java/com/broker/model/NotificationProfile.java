package com.broker.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class NotificationProfile {

    @Id
    @Column(name = "notification_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int uid;

    @Column(name = "profile_settings", nullable = false)
    private String profileSettings;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id")
    private Account notificationUser;

    public NotificationProfile(){

    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getProfileSettings() {
        return profileSettings;
    }

    public void setProfileSettings(String profileSettings) {
        this.profileSettings = profileSettings;
    }

    public Account getNotificationUser() {
        return notificationUser;
    }

    public void setNotificationUser(Account notificationUser) {
        this.notificationUser = notificationUser;
    }

    @Override
    public String toString() {
        return "NotificationProfile{" +
                "uid=" + uid +
                ", profileSettings='" + profileSettings + '\'' +
                ", notificationUser=" + notificationUser +
                '}';
    }
}
