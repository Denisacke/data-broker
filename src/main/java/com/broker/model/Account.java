package com.broker.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Account {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "account_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int uid;

    @Column(name = "name", nullable = false, unique = true)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "dateOfBirth", nullable = false)
    private LocalDate dateOfBirth;

    @Column(name = "height", nullable = false)
    private int height;

    @Column(name = "weight", nullable = false)
    private int weight;

    @Column(name = "sex", nullable = false)
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Column(name = "activity_level", nullable = false)
    @Enumerated(EnumType.STRING)
    private ActivityLevel activityLevel;

    @Column(name = "calories", nullable = false)
    private Integer requiredCalories;

    @Column(name = "water", nullable = false)
    private Integer waterIntake;

    @OneToMany(mappedBy = "account")
    private List<Recipe> recipes;

    @OneToMany(mappedBy = "user")
    private List<RecentProduct> recentProducts;

    @OneToMany(mappedBy = "activeUser")
    private List<RecentActivity> recentActivities;

    @OneToMany(mappedBy = "userStatistic")
    private List<Statistic> statistics;

    @OneToOne(mappedBy = "notificationUser")
    private NotificationProfile notificationProfile;

    public Account() {
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public ActivityLevel getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(ActivityLevel activityLevel) {
        this.activityLevel = activityLevel;
    }

    public List<Recipe> getCookedProducts() {
        return recipes;
    }

    public void setCookedProducts(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    public List<RecentProduct> getRecentProducts() {
        return recentProducts;
    }

    public void setRecentProducts(List<RecentProduct> recentProducts) {
        this.recentProducts = recentProducts;
    }

    public Integer getRequiredCalories() {
        return requiredCalories;
    }

    public void setRequiredCalories(Integer requiredCalories) {
        this.requiredCalories = requiredCalories;
    }

    public Integer getWaterIntake() {
        return waterIntake;
    }

    public void setWaterIntake(Integer water) {
        this.waterIntake = water;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<Statistic> getStatistics() {
        return statistics;
    }

    public void setStatistics(List<Statistic> statistics) {
        this.statistics = statistics;
    }

    public NotificationProfile getNotificationProfile() {
        return notificationProfile;
    }

    public void setNotificationProfile(NotificationProfile notificationProfile) {
        this.notificationProfile = notificationProfile;
    }

    @Override
    public String toString() {
        return "Account{" +
                "uid=" + uid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", height=" + height +
                ", weight=" + weight +
                ", sex=" + sex +
                ", activityLevel=" + activityLevel +
                ", requiredCalories=" + requiredCalories +
                ", waterIntake=" + waterIntake +
                ", cookedProducts=" + recipes +
                ", recentProducts=" + recentProducts +
                '}';
    }
}
