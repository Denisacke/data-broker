package com.broker.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Statistic {

    @Id
    @Column(name = "consumption_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int uid;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    @Column(name = "waterIntake", nullable = false)
    private int waterIntake;

    @Column(name = "calorieIntake", nullable = false)
    private int calorieIntake;

    @Column(name = "caloriesBurned", nullable = false)
    private int caloriesBurned;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account userStatistic;

    public Statistic(){

    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getCalorieIntake() {
        return calorieIntake;
    }

    public void setCalorieIntake(int calorieIntake) {
        this.calorieIntake = calorieIntake;
    }

    public int getCaloriesBurned() {
        return caloriesBurned;
    }

    public void setCaloriesBurned(int caloriesBurned) {
        this.caloriesBurned = caloriesBurned;
    }

    public int getWaterIntake() {
        return waterIntake;
    }

    public void setWaterIntake(int waterIntake) {
        this.waterIntake = waterIntake;
    }

    public Account getUserStatistic() {
        return userStatistic;
    }

    public void setUserStatistic(Account userStatistic) {
        this.userStatistic = userStatistic;
    }

    @Override
    public String toString() {
        return "Statistic{" +
                "uid=" + uid +
                ", date=" + date +
                ", waterIntake=" + waterIntake +
                ", calorieIntake=" + calorieIntake +
                ", caloriesBurned=" + caloriesBurned +
                ", userStatistic=" + userStatistic +
                '}';
    }
}
