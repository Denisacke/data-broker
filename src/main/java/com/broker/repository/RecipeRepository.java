package com.broker.repository;

import com.broker.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecipeRepository extends JpaRepository<Recipe, Integer> {

    Recipe findRecipeByName(String name);

    List<Recipe> findRecipeByAccount_Uid(Integer accountId);

    Integer deleteRecipeByNameAndAccount_Uid(String recipeName, Integer accountId);
}
