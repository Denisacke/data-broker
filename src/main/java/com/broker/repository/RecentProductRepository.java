package com.broker.repository;

import com.broker.model.Product;
import com.broker.model.RecentProduct;
import com.broker.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface RecentProductRepository extends JpaRepository<RecentProduct, Integer> {

    List<RecentProduct> findAllByDate(LocalDate date);

    List<RecentProduct> findAllByProduct(Product product);

    List<RecentProduct> findAllByRecipe(Recipe recipe);
}
