package com.broker.repository;

import com.broker.model.Activity;
import com.broker.model.RecentActivity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {

    List<Activity> findAllByCategory(String category);

    List<Activity> findAllByNameContaining(String name);

    Activity findActivityByUid(Integer uid);

    Activity findActivityByRecentActivitiesContaining(RecentActivity recentActivity);
}
