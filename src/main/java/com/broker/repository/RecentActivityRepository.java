package com.broker.repository;

import com.broker.model.RecentActivity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecentActivityRepository extends JpaRepository<RecentActivity, Integer> {

    List<RecentActivity> findRecentActivitiesByActiveUser_Uid(Integer accountId);
}
