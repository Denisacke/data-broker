package com.broker.repository;

import com.broker.model.NotificationProfile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NotificationProfileRepository extends JpaRepository<NotificationProfile, Integer> {

    Optional<NotificationProfile> findNotificationProfileByNotificationUser_Uid(Integer accountId);
}
