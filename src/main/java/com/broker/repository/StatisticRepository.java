package com.broker.repository;

import com.broker.model.Statistic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface StatisticRepository extends JpaRepository<Statistic, Integer> {

    List<Statistic> findAllByUserStatistic_Uid(Integer accountId);

    Optional<Statistic> findTopStatisticByDateAndUserStatistic_Uid(LocalDate date, Integer accountId);
}
