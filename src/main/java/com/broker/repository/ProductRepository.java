package com.broker.repository;

import com.broker.model.Product;
import com.broker.model.RecentProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {

//    Product findProductByRecentProducts(List<RecentProduct> recentProductList);

//    Product findProductByRecentProductsContaining(RecentProduct recentProduct);

    Product findProductByName(String name);
}
