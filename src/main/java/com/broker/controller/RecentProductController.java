package com.broker.controller;

import com.broker.model.Product;
import com.broker.model.RecentProduct;
import com.broker.model.dto.CreateProductDTO;
import com.broker.model.dto.MacroProductDTO;
import com.broker.model.dto.RecentProductDTO;
import com.broker.service.RecentProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/consumption")
public class RecentProductController {

    private final RecentProductService recentProductService;

    @Autowired
    public RecentProductController(RecentProductService recentProductService){
        this.recentProductService = recentProductService;
    }

    @GetMapping("{id}")
    public Pair<Integer, Integer> getDailyConsumption(@PathVariable("id") Integer accountId){

        return recentProductService.getTodayConsumptionForUser(accountId);
    }

    @GetMapping("all/{id}")
    public ResponseEntity<List<CreateProductDTO>> getRecentProductsForUser(@PathVariable("id") Integer accountId){

        return new ResponseEntity<>(recentProductService.getRecentlyConsumedProductsForUser(accountId), HttpStatus.OK);
    }

    @GetMapping("macro/{id}")
    public ResponseEntity<MacroProductDTO> getDailyMacros(@PathVariable("id") Integer accountId){

        return new ResponseEntity<>(recentProductService.getDailyMacrosForUser(accountId), HttpStatus.OK);
    }
    @PostMapping("")
    public ResponseEntity<RecentProductDTO> addRecentConsumption(@RequestBody RecentProductDTO recentProduct){

        return new ResponseEntity<>(recentProductService.addRecentlyConsumedProduct(recentProduct), HttpStatus.CREATED);
    }
}
