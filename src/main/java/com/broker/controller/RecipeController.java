package com.broker.controller;

import com.broker.model.dto.RecipeDTO;
import com.broker.model.dto.CreateRecipeDTO;
import com.broker.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/recipe")
public class RecipeController {

    private final RecipeService recipeService;

    @Autowired
    public RecipeController(RecipeService recipeService){
        this.recipeService = recipeService;
    }

    @PostMapping()
    public ResponseEntity<CreateRecipeDTO> saveRecipe(@RequestBody CreateRecipeDTO cookedProductDTO){

        return new ResponseEntity<>(recipeService.saveCookedProduct(cookedProductDTO), HttpStatus.CREATED);
    }

    @GetMapping("{id}")
    public ResponseEntity<List<RecipeDTO>> getRecipesByAccountId(@PathVariable("id") Integer accountId){

        return new ResponseEntity<>(recipeService.getCookedProducts(accountId), HttpStatus.OK);
    }

    @PostMapping("delete")
    public ResponseEntity<RecipeDTO> deleteRecipe(@RequestBody RecipeDTO recipeDTO){

        return new ResponseEntity<>(recipeService.deleteRecipe(recipeDTO), HttpStatus.OK);
    }
}
