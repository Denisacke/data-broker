package com.broker.controller;

import com.broker.model.dto.RecentActivityDTO;
import com.broker.service.RecentActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/activity")
public class RecentActivityController {

    private final RecentActivityService recentActivityService;

    @Autowired
    public RecentActivityController(RecentActivityService recentActivityService){
        this.recentActivityService = recentActivityService;
    }

    @PostMapping
    public ResponseEntity<RecentActivityDTO> saveRecentActivity(@RequestBody RecentActivityDTO recentActivityDTO){
        return new ResponseEntity<>(recentActivityService.saveRecentActivity(recentActivityDTO), HttpStatus.OK);
    }

    @GetMapping("account/{id}")
    public ResponseEntity<List<RecentActivityDTO>> getRecentActivitiesForUser(@PathVariable("id") Integer accountId){

        return new ResponseEntity<>(recentActivityService.getRecentActivitiesForUser(accountId), HttpStatus.OK);
    }
}
