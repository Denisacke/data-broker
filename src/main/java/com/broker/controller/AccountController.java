package com.broker.controller;

import com.broker.model.Account;
import com.broker.model.User;
import com.broker.model.dto.AccountDTO;
import com.broker.model.dto.CreateAccountDTO;
import com.broker.service.AccountService;
import com.broker.service.NotificationProfileService;
import com.broker.service.RecentActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
@RequestMapping("/account")
public class AccountController {

    private final AccountService accountService;
    private final RecentActivityService recentActivityService;
    private final NotificationProfileService notificationProfileService;

    @Autowired
    public AccountController(AccountService accountService,
                             RecentActivityService recentActivityService,
                             NotificationProfileService notificationProfileService){
        this.accountService = accountService;
        this.recentActivityService = recentActivityService;
        this.notificationProfileService = notificationProfileService;
    }

    @PostMapping("/login")
    public ResponseEntity<AccountDTO> getAccount(@RequestBody User user) {
        AccountDTO account = accountService.getAccountByAuthDetails(user);

        if(account == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        System.out.println(account);
        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<AccountDTO> insertAccount(@RequestBody CreateAccountDTO account) {
        AccountDTO entry = accountService.saveAccount(account);

        return new ResponseEntity<>(entry, HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<AccountDTO> updateAccount(@RequestBody AccountDTO accountDTO){

        return new ResponseEntity<>(accountService.updateAccount(accountDTO), HttpStatus.CREATED);
    }

    @GetMapping("/caloriesburned/{id}")
    public ResponseEntity<Integer> getCaloriesBurnedForUser(@PathVariable("id") Integer accountId){
        return new ResponseEntity<>(recentActivityService.getTodayCaloriesBurnedForUser(accountId), HttpStatus.OK);
    }
}
