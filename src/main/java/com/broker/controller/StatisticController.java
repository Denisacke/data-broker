package com.broker.controller;

import com.broker.model.RecentActivity;
import com.broker.model.dto.GraphStatisticDTO;
import com.broker.model.dto.StatisticDTO;
import com.broker.service.AccountService;
import com.broker.service.RecentActivityService;
import com.broker.service.RecentProductService;
import com.broker.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/statistics")
public class StatisticController {

    private StatisticService statisticService;
    private AccountService accountService;
    private RecentProductService recentProductService;
    private RecentActivityService recentActivityService;

    @Autowired
    public StatisticController(StatisticService statisticService,
                               AccountService accountService,
                               RecentProductService recentProductService,
                               RecentActivityService recentActivityService){
        this.statisticService = statisticService;
        this.accountService = accountService;
        this.recentActivityService = recentActivityService;
        this.recentProductService = recentProductService;
    }

    @GetMapping("/submit")
    public ResponseEntity<List<StatisticDTO>> testCron(){
        List<Integer> accountIdList = accountService.getAllAccountIds();
        for(Integer id : accountIdList){
            Pair<Integer, Integer> consumption = recentProductService.getTodayConsumptionForUser(id);
            Integer caloriesBurned = recentActivityService.getTodayCaloriesBurnedForUser(id);
            statisticService.saveStatistics(consumption, caloriesBurned, id);
        }

        return new ResponseEntity<>(statisticService.getAllStatistics(), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<List<GraphStatisticDTO>> getStatisticsForUser(@PathVariable("id") Integer accountId){

        //Update statistics for today
        Pair<Integer, Integer> consumption = recentProductService.getTodayConsumptionForUser(accountId);
        Integer caloriesBurned = recentActivityService.getTodayCaloriesBurnedForUser(accountId);
        statisticService.saveStatistics(consumption, caloriesBurned, accountId);

        //Get statistics for user
        return new ResponseEntity<>(statisticService.getStatisticsForUser(accountId), HttpStatus.OK);
    }
}
