package com.broker.controller;

import com.broker.model.dto.NotificationProfileDTO;
import com.broker.service.NotificationProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/notification")
public class NotificationProfileController {

    private final NotificationProfileService notificationProfileService;

    @Autowired
    public NotificationProfileController(NotificationProfileService notificationProfileService){
        this.notificationProfileService = notificationProfileService;
    }

    @GetMapping("{id}")
    public ResponseEntity<NotificationProfileDTO> getNotificationProfileForUser(@PathVariable("id") Integer accountId){

        return new ResponseEntity<>(notificationProfileService.getNotificationProfileForUser(accountId), HttpStatus.OK);
    }
    @PutMapping()
    public ResponseEntity<NotificationProfileDTO> updateNotificationProfile(@RequestBody NotificationProfileDTO notificationProfileDTO){

        return new ResponseEntity<>(notificationProfileService.updateNotificationProfile(notificationProfileDTO), HttpStatus.OK);
    }
}
