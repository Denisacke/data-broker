package com.broker.controller;

import com.broker.model.Product;
import com.broker.model.dto.AccountDTO;
import com.broker.model.dto.ProductDTO;
import com.broker.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService){
        this.productService = productService;
    }

    @PostMapping()
    public ResponseEntity<ProductDTO> addProduct(@RequestBody Product product){

        return new ResponseEntity<>(productService.saveProduct(product), HttpStatus.CREATED);
    }
}
