package com.broker.controller;

import com.broker.model.Activity;
import com.broker.model.dto.ActivityDTO;
import com.broker.model.dto.RecentActivityDTO;
import com.broker.service.ActivityService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("/activity")
public class ActivityController {

    private ActivityService activityService;

    @Autowired
    public ActivityController(ActivityService activityService){
        this.activityService = activityService;
    }

    private boolean isActivityNameAlreadyExistentForDifferentCategory(ArrayList<Activity> activityList, String name, String category){
        for(Activity activity : activityList){
            if(activity.getName().equalsIgnoreCase(name) && !activity.getCategory().equals(category)){
                return true;
            }
        }

        return false;
    }
    private Set<String> saveActivities(){
        ArrayList<Activity> activityList = new ArrayList<>();
        ArrayList<Activity> finalActivityList = new ArrayList<>();
        try {
            String url = "https://golf.procon.org/met-values-for-800-activities/";
            Document doc = Jsoup.connect(url).get();
            Elements data = doc.select("tr").next();
            int size = data.size();
            for (int i = 0; i < size; i++) {
                String category = data.select("tr").eq(i).select("td").eq(0).text().replace("\\/", ",");
                String activityName = data.select("tr").eq(i).select("td").eq(1).text();
                Double met = Double.parseDouble(data.select("tr").eq(i).select("td").eq(2).text());

                String name = activityName.split(",|\\(")[0].trim().toLowerCase();
                if(isActivityNameAlreadyExistentForDifferentCategory(activityList, name, category)){
                    continue;
                }
                Activity activity = new Activity();
                activity.setCategory(category);
                activity.setName(name);
                activity.setMetCoefficient(met);

                activityList.add(activity);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        activityList.sort((o1, o2) -> {
            if(o1.getCategory().compareTo(o2.getCategory()) == 0){
                return o1.getName().compareTo(o2.getName());
            }

            return o1.getCategory().compareTo(o2.getCategory());
        });

        String currentActivityName = "";
        String currentCategory = "";
        Double currentMet = 0.0;
        int sameActivityNumber = 0;

        for(Activity currentActivity : activityList){
            String name = currentActivity.getName();
            if(currentActivityName.contains(name)){
                currentMet += currentActivity.getMetCoefficient();
                sameActivityNumber++;
            }else{
                if(!currentActivityName.isEmpty()){
                    Activity activity = new Activity();
                    activity.setName(currentActivityName);
                    activity.setMetCoefficient(Math.round(currentMet / sameActivityNumber * 100.0) / 100.0);
                    activity.setCategory(currentCategory);
                    activity.setUid(finalActivityList.size());
                    finalActivityList.add(activity);
                }

                currentActivityName = name;
                currentMet = currentActivity.getMetCoefficient();
                currentCategory = currentActivity.getCategory();
                sameActivityNumber = 1;
            }

        }

        activityService.saveActivityList(finalActivityList);
        return finalActivityList.stream().map(Activity::getCategory).collect(Collectors.toSet());
    }

    @GetMapping("/all")
    public Set<String> testScraper(){
        Set<String> finalSet = saveActivities();

        return finalSet;
    }

    @GetMapping
    public Set<String> getAllActivities(){

        try{
            Set<String> activities = activityService.getAllActivities().stream().map(Activity::getCategory).collect(Collectors.toSet());
            if(activities.isEmpty()){
                return saveActivities();
            }
            return activities;
        }catch (Exception e){
            return saveActivities();
        }

    }

    @GetMapping("/category/{category}")
    public ResponseEntity<List<ActivityDTO>> getAllActivitiesByCategory(@PathVariable String category){
        return new ResponseEntity<>(activityService.getAllActivitiesByCategory(category), HttpStatus.OK);
    }

    @GetMapping("/search/{activity}")
    public ResponseEntity<List<ActivityDTO>> getActivity(@PathVariable("activity") String activityName){
        return new ResponseEntity<>(activityService.getActivitiesByName(activityName), HttpStatus.OK);
    }
}
