package com.broker.cron;

import com.broker.model.Statistic;
import com.broker.service.AccountService;
import com.broker.service.RecentActivityService;
import com.broker.service.RecentProductService;
import com.broker.service.StatisticService;
import org.hibernate.internal.build.AllowPrintStacktrace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
public class CronDatabaseChange {

    private final RecentProductService recentProductService;
    private final RecentActivityService recentActivityService;
    private final StatisticService statisticService;
    private final AccountService accountService;

    @Autowired
    public CronDatabaseChange(RecentProductService recentProductService,
                              RecentActivityService recentActivityService,
                              StatisticService statisticService,
                              AccountService accountService){
        this.recentActivityService = recentActivityService;
        this.recentProductService = recentProductService;
        this.statisticService = statisticService;
        this.accountService = accountService;
    }

    @Scheduled(cron = "0 0 17,22 * * *")
    public void moveCalorieRecords(){
        List<Integer> accountIdList = accountService.getAllAccountIds();
        for(Integer id : accountIdList){
            Pair<Integer, Integer> consumption = recentProductService.getTodayConsumptionForUser(id);
            Integer caloriesBurned = recentActivityService.getTodayCaloriesBurnedForUser(id);
            statisticService.saveStatistics(consumption, caloriesBurned, id);
        }
    }
}
